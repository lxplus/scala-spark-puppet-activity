package ch.cern.config

import ch.cern.monitoring.config.Config.parseConfig
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.json4s.CustomSerializer
import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.native.Serialization._
import scalaj.http.Http

case class PuppetActivity(population: Int,
                        producer : String = "kpi",
                       `type` : String = "service",
                       serviceid : String = "cfg" ,
                       service_status : String = "available")  {}


class PuppetActivitySerializer extends CustomSerializer[PuppetActivity] (format => ( {

  case json: JValue =>
    implicit val formats = format
    val population = (json \ "population").extract[Int]
    val producer = (json \ "producer").extract[String]
    val `type` = (json \ "type").extract[String]
    val serviceid = (json \ "serviceid").extract[String]
    val service_status = (json \ "service_status").extract[String]



   PuppetActivity(population, producer, `type`, serviceid, service_status)
} , {
  case puppetActivity: PuppetActivity =>
    implicit val formats = format
      ("population" -> puppetActivity.population.toString) ~
      ("producer" -> puppetActivity.producer) ~
      ("type" -> puppetActivity.`type`) ~
      ("serviceid" -> puppetActivity.serviceid) ~
      ("service_status" -> puppetActivity.service_status)

}))


object PuppetActivity {

  def main(args: Array[String]) {

    val APPLICATION_NAME = "Spark Puppet Activity"
    val config = parseConfig(args, APPLICATION_NAME)

    var sparkConf = new SparkConf()

    val spark = SparkSession
      .builder
      .appName(APPLICATION_NAME)
      .config(sparkConf)
      .getOrCreate()

    spark.conf.set("spark.sql.streaming.stateStore.minDeltasForSnapshot", 2)
    spark.conf.set("spark.sql.streaming.minBatchesToRetain", 3)
    spark.conf.set("spark.sql.shuffle.partitions", 50)
    spark.conf.set("spark.sql.streaming.schemaInference", value = true)

    import spark.implicits._
    val puppetDataRDD =
      spark
      .read
      .format("kafka")
      .option("kafka.bootstrap.servers", config.kafkaInputBrokers)
      .option("subscribe", "collectd_raw_service")
      .option("startingOffsets", "earliest")
      .load()
      .select($"value".cast("string"))
      .map(row => row.getAs[String]("value"))


  puppetDataRDD.show(false)


  implicit val formats = org.json4s.DefaultFormats + new PuppetActivitySerializer

   val puppetHosts =
        spark
        .read
        .json(puppetDataRDD)
        .where("data.plugin_instance = 'puppetdb_population_num_nodes'")
        .where("metadata.submitter_hostgroup = 'punch/pdb/puppetdb/two/sync'")
        .select($"data.*")
        .groupBy("host" )
          .agg(last("value").alias("population"))
        .map(row => PuppetActivity(population = row.getAs[Double]("population").toInt))
        .collect()


    puppetHosts.foreach(activity => {
      System.out.println(write(activity))
      Http("http://monit-metrics.cern.ch:10012")
      .postData("[" + write(activity) + "]")
      .asString.code

    })


  }
}
