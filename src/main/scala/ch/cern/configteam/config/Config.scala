package ch.cern.monitoring.config

object Config {

  val KAFKA_INPUT_BROKERS = Param(
    'i',
    "input-brokers-url",
    "URLs of kafka brokers for subscribing",
    "monit-kafka.cern.ch:9092"
  )
  val KAFKA_OUTPUT_BROKERS = Param(
    'o',
    "output-brokers-url",
    "URLs of kafka brokers for publishing",
    "monit-kafka.cern.ch:9092"
  )


  case class Config(kafkaInputBrokers: String = KAFKA_INPUT_BROKERS.default,
                    kafkaOutputBrokers: String = KAFKA_OUTPUT_BROKERS.default
                   )

  def parseConfig(args: Array[String], applicationName: String): Config = {
    val parser = new scopt.OptionParser[Config](applicationName) {
      head(applicationName, getClass.getPackage.getImplementationVersion)
      opt[String](KAFKA_INPUT_BROKERS.abbr, KAFKA_INPUT_BROKERS.fullName)
        .action((v, c) => c.copy(kafkaInputBrokers = v))
        .text(KAFKA_INPUT_BROKERS.text)
      opt[String](KAFKA_OUTPUT_BROKERS.abbr, KAFKA_OUTPUT_BROKERS.fullName)
        .action((v, c) => c.copy(kafkaOutputBrokers = v))
        .text(KAFKA_OUTPUT_BROKERS.text)

    }
    parser.parse(args, Config()) match {
      case Some(config) =>
        // do stuff
        config;
      case None =>
        // arguments are bad, error message will have been displayed
        System.exit(1)
        null
    }
  }
}
