package ch.cern.monitoring.config

case class Param[T](abbr: Char, fullName: String, text: String, default: T) {}